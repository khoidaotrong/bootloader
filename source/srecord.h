#ifndef _SRECORD_H_
#define _SRECORD_H_

#include <stdbool.h>
#include <stdint.h>

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define SRECORD_MAXIMUM_BYTES_OF_DATA         252
#define SRECORD_START_OF_LINE_CHARACTER       'S'
#define SRECORD_END_OF_LINE_CHARACTER         '\n'

typedef enum
{
    PARSE_STATUS_START,
    PARSE_STATUS_PROCESSING,
    PARSE_STATUS_UNSUPPORT,
    PARSE_STATUS_DONE,
    PARSE_STATUS_ERROR
} srec_parsing_status_t;

typedef struct
{
    uint32_t address;
    uint8_t data[SRECORD_MAXIMUM_BYTES_OF_DATA];
    uint8_t data_length;
    char log[50];
} srec_data_struct_t;

/*******************************************************************************
 * API
 ******************************************************************************/
/*!
* parse a srecord line
* param[1] input srecord line
* param[2] output data struct of srecord line
* return status after parsing 
*/
srec_parsing_status_t srecord_parse_srecorc_line(uint8_t * srecord_line, srec_data_struct_t * srec_data);

#endif /* _SRECORD_H_ */
