#ifndef _QUEUE_H_
#define _QUEUE_H_

#include <stdbool.h>
#include <stdint.h>


/*******************************************************************************
 * Definitions
 ******************************************************************************/


/*******************************************************************************
 * API
 ******************************************************************************/
/*!
* queue init, reset all variable in queue
*/
void queue_init();

/*!
* check if queue is empty
* return 1 if queue is empty
* return 0 if queue is not empty
*/
bool queue_is_empty();

/*!
* push single char to current element
* param[1] input char
*/
void queue_push_char(uint8_t in_char);

/*!
* push data to current element completed
* start push data of new element
* return 0 when queue is full
* return 1 if success
*/
bool queue_push_new_element();

/*!
* get data of head element
* param[1] output pointer to data of head element
*/
void queue_peek_head_element(uint8_t ** out_element);

/*!
* remove head element after peek
*/
void queue_remove_head_element();

#endif /* _QUEUE_H_ */
