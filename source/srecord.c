#include "srecord.h"
#include <stdio.h>

/*******************************************************************************
 * Definitions
 ******************************************************************************/


/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static uint8_t srec_ascii_to_hex(uint8_t in_char);


/*******************************************************************************
 * Variables
 ******************************************************************************/


/*******************************************************************************
 * Code
 ******************************************************************************/
uint8_t srec_ascii_to_hex(uint8_t in_char){
    uint8_t hex = 0xFF;
    
    if((in_char >= '0') && (in_char <= '9'))
    {
        hex = in_char - 0x30;
    }
    else if((in_char >= 'A') && (in_char <= 'F'))
    {
        hex = in_char - 0x37;
    }
    
    return hex;
}

srec_parsing_status_t srecord_parse_srecorc_line(uint8_t * srecord_line, srec_data_struct_t * srec_data)
{
    srec_parsing_status_t return_status = PARSE_STATUS_ERROR;
    uint8_t srec_type = 0xff;
    uint8_t number_of_address_bytes = 0;
    uint8_t byte_count = 0;
    uint8_t checksum = 0;
    uint8_t data_temp = 0;
    uint16_t offset = 4;
    uint16_t index = 0;
    
    if(srecord_line[0] == SRECORD_START_OF_LINE_CHARACTER)
    {
        /* get srec type */
        srec_type = srec_ascii_to_hex(srecord_line[1]);
        switch(srec_type)
        {
        case 0:
            return_status = PARSE_STATUS_START;
            break;
        case 1:
        case 2:
        case 3:
            return_status = PARSE_STATUS_PROCESSING;
            number_of_address_bytes = srec_type + 1;
            break;
        case 5:
        case 6:
            return_status = PARSE_STATUS_UNSUPPORT;
            break;
        case 7:
        case 8:
        case 9:
            number_of_address_bytes = 11 - srec_type;
            return_status = PARSE_STATUS_DONE;
            break;
        default:
            break;
        }
        
        /* get byte count */
        byte_count = srec_ascii_to_hex(srecord_line[2]) << 4 | srec_ascii_to_hex(srecord_line[3]);
        checksum += byte_count;
        
        /* get address */
        srec_data->address = 0;
        for (index = 0; index < number_of_address_bytes; index++)
        {
            data_temp = (srec_ascii_to_hex(srecord_line[offset]) << 4) | srec_ascii_to_hex(srecord_line[offset + 1]);
            srec_data->address = (srec_data->address << 8) | data_temp;
            checksum += data_temp;
            offset += 2;
        }
        
        /* get data */
        srec_data->data_length = (byte_count - number_of_address_bytes - 1);
        for (index = 0; index < srec_data->data_length; index++)
        {
            data_temp = (srec_ascii_to_hex(srecord_line[offset]) << 4) | srec_ascii_to_hex(srecord_line[offset + 1]);
            srec_data->data[index] = data_temp;
            checksum += data_temp;
            offset += 2;
        }
        
        /* get checksum */
        checksum += (srec_ascii_to_hex(srecord_line[offset]) << 4) | srec_ascii_to_hex(srecord_line[offset + 1]);
        if (0xFF != checksum)
        {
            return_status = PARSE_STATUS_ERROR;
        }
    }
    
    return return_status;
}
