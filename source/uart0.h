#ifndef _UART0_H_
#define _UART0_H_

#include <stdint.h>

/*******************************************************************************
 * Definitions
 ******************************************************************************/
/*! number of data bit typedef */
typedef enum
{
    BITS_DATA_8,
    BITS_DATA_9,
    BITS_DATA_10,
} uart_number_of_data_bit_t;

/*! parity type typedef */
typedef enum
{
    NONE_PARITY,
    ODD_PARITY,
    EVEN_PARITY,
} uart_parity_type_t;

/*! number of stop bit typedef */
typedef enum
{
    BIT_STOP_1,
    BITS_STOP_2,
} uart_number_of_stop_bit_t;

/*! return status of uart init function typedef */
typedef enum
{
    INIT_SUCCESS,
    BAUDRATE_NOT_SUPPORT,
    NUMBER_OF_BITS_DATA_NOT_SUPPORT,
} uart_init_status_t;

/*! status of received data typedef */
typedef enum
{
    DATA_CORRECT,
    DATA_INCORRECT,
} uart_Rx_status_t;

/*!
* uart config parameter typedef
*/
typedef struct
{
    uint32_t                  baudrate;
    uart_number_of_data_bit_t number_of_data_bit;
    uart_parity_type_t        parity_type;
    uart_number_of_stop_bit_t number_of_stop_bit;
} uart_init_struct_t;

/*!
* uart rx handle typedef
*/
typedef struct
{
    uint8_t data;
    uart_Rx_status_t status;
} uart_Rx_handle_struct_t;

/*! pointer to callback function */
typedef void (*uart0_Rx_interrupt_callback_fptr_t)(uart_Rx_handle_struct_t);

/*******************************************************************************
 * API
 ******************************************************************************/
/*!
* uart0 init
* param[1] input init struct
* param[2] input module frequency (Hz)
* return status when init module
*/
uart_init_status_t uart0_init(uart_init_struct_t init_struct, uint32_t clock_frequency);

/*!
* set Rx interrupt callback function
* param[1] input callback function pointer
*/
void uart0_set_Rx_interrupt_callback(uart0_Rx_interrupt_callback_fptr_t call_back_function);

/*!
* uart0 send single char
* param[1] input char
*/
void uart0_put_char(char inChar);

/*!
* uart0 send string
* param[1] input string
*/
void uart0_put_string(char * inStr);

void uart0_disable_interupt();

void uart0_enable_interupt();


#endif /* _UART0_H_ */
