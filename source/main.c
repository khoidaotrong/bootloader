/*******************************************************************************
* file: main.c
* hold button sw1 while press reset button will start the boot program
*    boot program: 
*        receive srecord file sent from PC via UART 115200, frame 8N1
*        parse srecord file, write Application program to flash from address A000
* leave button sw1 while press reset button will start the Application program
*******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "MKL46Z4.h"
#include "uart0.h"
#include "queue.h"
#include "srecord.h"
#include "flash.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define APPLICATION_START_ADDRESS                              0xA000
#define APPLICATION_INTERRUPT_VECTOR_TABLE_START_ADDRESS       0xA000
#define APPLICATION_RESET_HANDLER_ADDRESS                      0xA004

/* sw1 use as boot button */
#define BOOT_BUTTON_STATUS                    (((GPIOC->PDIR)>>3) & 1U)

/*! typedef a pointer to application's reset handler */
typedef void (*app_reset_handler_fptr_t)(void);

typedef enum
{
    UART_RX_ERROR,
    QUEUE_FULL,
    SRECORD_PARSE_ERROR,
    ERROR_ADDRESS,
} boot_error_code_t;

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
/*!
* uart0 Rx interrupt callback function
* param[1] input uart receive handle struct
*/
void boot_uart0_Rx_ISR_callback(uart_Rx_handle_struct_t Rx_handle);

/*!
* work as a queue to flash data
* wait for 4 bytes before write them to flash
*/
void boot_push_data_to_flash(uint8_t data);

/*!
* setup sw1 as input
*/
void boot_init_boot_button();

/*! stop program if get an error */
void boot_error_handler(boot_error_code_t error_code);

/*******************************************************************************
 * Variables
 ******************************************************************************/
//static uint32_t number_of_bytes_programed = 0;
static uint32_t current_flash_address = APPLICATION_START_ADDRESS;

static uint8_t block_write_data[4];
/* index of current in block_write_data */
static uint8_t index = 0;

/*******************************************************************************
 * Code
 ******************************************************************************/
void main()
{
  char log[50];
  
    app_reset_handler_fptr_t app_reset_handler;
    
    srec_data_struct_t srecord_data;
    srec_parsing_status_t srecord_status;
    uint8_t *strCurrentLine;
    
    uart_init_struct_t uart0_init_struct;
    uint8_t count;
    
    /* boot button init */
    boot_init_boot_button();
    
    /* init uart0 */
    uart0_init_struct.baudrate = 9600;
    uart0_init_struct.number_of_data_bit = BITS_DATA_8;
    uart0_init_struct.number_of_stop_bit = BIT_STOP_1;
    uart0_init_struct.parity_type = NONE_PARITY;
    
    uart0_init(uart0_init_struct, DEFAULT_SYSTEM_CLOCK);
    
    uart0_put_string("boot start\n");
    
    /* set callback function */
    uart0_set_Rx_interrupt_callback(boot_uart0_Rx_ISR_callback);
    
    /* if boot button is pressed */
    if(BOOT_BUTTON_STATUS == 0)
    {
        /* init queue */
        queue_init();
        
        uart0_put_string("Ready to program new application.\n");
        uart0_put_string("Let's send srecord file.\n");
        
        while(1)
        {
            if(!queue_is_empty())
            {
                /* peek head element */
                queue_peek_head_element(&strCurrentLine);
                
                /* srecord parsing */
                srecord_status = srecord_parse_srecorc_line(strCurrentLine, &srecord_data);
                
                if(srecord_status == PARSE_STATUS_PROCESSING)
                {
                    if(srecord_data.address == current_flash_address)
                    {
                        for(count = 0; count < srecord_data.data_length; count++)
                        {
                            boot_push_data_to_flash(srecord_data.data[count]);
                        }
                    }
                    else if(srecord_data.address > current_flash_address)
                    {
                        while(index != 0)
                        {
                            /* padding 0 */
                            boot_push_data_to_flash(0x00);
                        }
                        
                        current_flash_address = srecord_data.address;
                        
                        for(count = 0; count < srecord_data.data_length; count++)
                        {
                            boot_push_data_to_flash(srecord_data.data[count]);
                        }
                    }
                    else
                    {
                        boot_error_handler(ERROR_ADDRESS);
                    }
                    uart0_put_string(">>");
                }
                else if(srecord_status == PARSE_STATUS_DONE)
                {
                    while(index != 0)
                    {
                        /* padding 0 */
                        boot_push_data_to_flash(0x00);
                    }
                    uart0_put_string(strCurrentLine);
                    uart0_put_string("\nProgram done!\n");
                    uart0_put_string("Please leave sw1 and press reset button to start application.\n");
                    
                    /* boot success */
                    while(1);
                }
                else if(srecord_status == PARSE_STATUS_ERROR)
                {
                  uart0_put_string(strCurrentLine);
                  sprintf(log, "\n address %x \n", srecord_data.address);
    uart0_put_string(log);
    sprintf(log, "data length %d \n", srecord_data.data_length);
    uart0_put_string(log);
    
    for(count = 0; count < srecord_data.data_length; count++)
                        {
                          sprintf(log, " %x ",srecord_data.data[count]);
    uart0_put_string(log);
                        }
                    boot_error_handler(SRECORD_PARSE_ERROR);
                }
                
                queue_remove_head_element();
            }
        }
    }
    else
    {
        /* set VTOR for application */
        SCB->VTOR = APPLICATION_INTERRUPT_VECTOR_TABLE_START_ADDRESS;
        
        sprintf(log, "msp %x \n", Read_FlashAddress(APPLICATION_INTERRUPT_VECTOR_TABLE_START_ADDRESS));
    uart0_put_string(log);
        /* set MSP for application  */
        __set_MSP(Read_FlashAddress(APPLICATION_INTERRUPT_VECTOR_TABLE_START_ADDRESS));
        
        /* start application */
        app_reset_handler = (app_reset_handler_fptr_t)Read_FlashAddress(APPLICATION_RESET_HANDLER_ADDRESS);
        
        app_reset_handler();
        while(1);
    }
}

void boot_uart0_Rx_ISR_callback(uart_Rx_handle_struct_t Rx_handle)
{
    if(Rx_handle.status == DATA_CORRECT)
    {
        if(Rx_handle.data == SRECORD_END_OF_LINE_CHARACTER)
        {
            if(queue_push_new_element() == 0)
            {
                boot_error_handler(QUEUE_FULL);
            }
        }
        else
        {
            queue_push_char(Rx_handle.data);
        }
    }
    else
    {
        boot_error_handler(UART_RX_ERROR);
    }
}

void boot_push_data_to_flash(uint8_t data)
{
    if(((current_flash_address) % 1024) == 0)
    {
        //uart0_disable_interupt();
        Erase_Sector(current_flash_address);
        //uart0_enable_interupt();
    }
    
    current_flash_address++;
    block_write_data[index] = data;
    index++;
    if(index == 4)
    {
        index = 0;
        Program_LongWord_8B((current_flash_address - 4), block_write_data);
    }
}

void boot_init_boot_button()
{
    /* enable clock */
    SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK;
    
    /* set mux GPIO */
    PORTC->PCR[3] &= ~PORT_PCR_MUX_MASK;
    PORTC->PCR[3] |= PORT_PCR_MUX(1);
    
    /* enable pull up resister */
    PORTC->PCR[3] |= PORT_PCR_PE_MASK;
    PORTC->PCR[3] |= PORT_PCR_PS_MASK;
    
    /* GPIO set input */
    GPIOC->PDDR &= ~(1<<3);
}

void boot_error_handler(boot_error_code_t error_code)
{
    char log[30];
    sprintf(log, "Error %d \n", error_code);
    uart0_put_string(log);
    while(1);
}
