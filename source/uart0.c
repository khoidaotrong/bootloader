#include "MKL46Z4.h"
#include "core_cm0plus.h"
#include "uart0.h"
#include "queue.h"
#include <stdint.h>

/*******************************************************************************
 * Definitions
 ******************************************************************************/


/*******************************************************************************
 * Prototypes
 ******************************************************************************/


/*******************************************************************************
 * Variables
 ******************************************************************************/
uart_Rx_handle_struct_t Rx_handle;
uart0_Rx_interrupt_callback_fptr_t Rx_interrupt_callback_function;


/*******************************************************************************
 * Code
 ******************************************************************************/
uart_init_status_t uart0_init(uart_init_struct_t init_struct, uint32_t clock_frequency)
{
    uint8_t selected_osr = 0;
    uint8_t osr = 0;
    uint32_t sbr = 0;
    uint32_t baudDiff = 0xffffffff;
    uint32_t smallestBaudDiff = 0xffffffff;
    uint8_t temp;
    
    
    /* enable clock port A in SIM */
    SIM->SCGC5 |= SIM_SCGC5_PORTA(1);
    
    /* pin mux config */
    PORTA->PCR[1] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[1] |= PORT_PCR_MUX(2);
    
    PORTA->PCR[2] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[2] |= PORT_PCR_MUX(2);
    
    /* uart0 clock source select */
    SIM->SOPT2 &= ~SIM_SOPT2_UART0SRC_MASK;
    SIM->SOPT2 |= SIM_SOPT2_UART0SRC(1);
    
    /* enable clock uart in SIM */
    SIM->SCGC4 |= SIM_SCGC4_UART0(1);
    
    /* setup uart register */
    /* TE, RE must be clear to setup */
    UART0->C2 &= ~UART0_C2_TE_MASK;
    UART0->C2 &= ~UART0_C2_RE_MASK;
    
    /* baud calculate */
    for(osr = 3; osr<32; osr++)
    {
        sbr = clock_frequency / (init_struct.baudrate * (osr+1));
        if(clock_frequency/ (sbr * (osr+1)) <= init_struct.baudrate)
            baudDiff = init_struct.baudrate - clock_frequency/ (sbr * (osr+1));
        else
            baudDiff = clock_frequency/ (sbr * (osr+1)) - init_struct.baudrate;
        
        if(baudDiff <= smallestBaudDiff){
            smallestBaudDiff = baudDiff;
            selected_osr = osr;
        }
    }
    
    /* setup OSR */
    UART0->C4 = selected_osr;
    
    sbr = clock_frequency / (init_struct.baudrate * (selected_osr+1));
    
    /* set baud */
    UART0->BDL = (sbr & 0xff);
    UART0->BDH = ((sbr>>8) & 0xff);
    
    /* enable Rx, Tx to start UART */
    UART0->C2 |= UART0_C2_TE(1);
    UART0->C2 |= UART0_C2_RE(1);
    
    /*clear all flag*/
    UART0->S1 |= 0x1F;
    while(UART0->S1 & UART0_S1_RDRF_MASK){
        temp = UART0->D;
        /* assign to make compiler not warning */
        temp = temp;
    }
    
    /* enable receive interrupt */
    NVIC->ISER[0U] |= (unsigned int)(1UL << (UART0_IRQn));
    UART0->C2 |= UART0_C2_RIE(1);
}

void uart0_set_Rx_interrupt_callback(uart0_Rx_interrupt_callback_fptr_t call_back_function)
{
    Rx_interrupt_callback_function = call_back_function;
}

void uart0_put_char(char in_char)
{
    while(!((UART0->S1 & UART0_S1_TDRE_MASK) >> UART0_S1_TDRE_SHIFT))
    {
        /* wait for transmiss buffer empty */
    }
    UART0->D = in_char;
}

void uart0_put_string(char * in_str)
{
    while(*in_str != 0){
        uart0_put_char(*in_str++);
    }
}

void uart0_disable_interupt()
{
    NVIC->ICER[0U] |= (unsigned int)(1UL << (UART0_IRQn));
}

void uart0_enable_interupt()
{
    NVIC->ISER[0U] |= (unsigned int)(1UL << (UART0_IRQn));
}

/*******************************************************************************
 * UART0 IRQ Handler
 ******************************************************************************/
void UART0_IRQHandler()
{
    if(((UART0->S1 & UART0_S1_OR_MASK) >> UART0_S1_OR_SHIFT) & 0x1)
    {
        Rx_handle.status = DATA_INCORRECT;
    }
    else if(((UART0->S1 & UART0_S1_FE_MASK) >> UART0_S1_FE_SHIFT) & 0x1)
    {
        Rx_handle.status = DATA_INCORRECT;
    }
    else if(((UART0->S1 & UART0_S1_RDRF_MASK) >> UART0_S1_RDRF_SHIFT) & 0x1)
    {
        Rx_handle.status = DATA_CORRECT;
        Rx_handle.data = (uint8_t)(UART0->D);
    }
    
    Rx_interrupt_callback_function(Rx_handle);
}
